/**
 * \file vector.h
 * \brief generic vector library allowing to manage vectors generically.
 * \author sangsoic <sangsoic@protonmail.com>
 * \version 0.5
 * \date 2020-02-26 Wed 03:00 AM
 * \copyright 
 *
 * Copyright 2020 Sangsoic library author
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *             http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __VECTOR__
	#define __VECTOR__
	#include <stdio.h>
	#include <stdlib.h>
	#include <stdbool.h>
	#include <errno.h>
	#include <string.h>

	/**
	 * \def VECTOR
	 * \brief User-level working objects.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \date 2020-05-05 Tue 07:19 AM
	 * \param TYPE C datatype
	 * \param NAME Name to give to generic object
	 */
	#define VECTOR(TYPE, NAME) \
		typedef struct { \
			TYPE * value; \
			size_t cardinal; \
			size_t capacity; \
		} Vector##NAME;

	/**
	 * \def VECTOR_MALLOC
	 * \brief Wrapper routine for \a Vector_malloc routine.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \date 2020-05-05 Tue 07:21 AM
	 * \param TYPE C datatype
	 * \param NAME Name to give to generic object
	 */
	#define VECTOR_MALLOC(TYPE, NAME) \
		static Vector##NAME * Vector##NAME##_malloc(const size_t capacity) \
		{ \
			return (Vector##NAME *)Vector_malloc(capacity, sizeof(TYPE)); \
		}

	/**
	 * \def VECTOR_REALLOC
	 * \brief Wrapper routine for \a Vector_realloc routine.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \date 2020-05-05 Tue 07:21 AM
	 * \param TYPE C datatype
	 * \param NAME Name to give to generic object
	 */
	#define VECTOR_REALLOC(TYPE, NAME) \
		static Vector##NAME * Vector##NAME##_realloc(Vector##NAME * vector, const size_t capacity) \
		{ \
			return (Vector##NAME *)Vector_realloc((Vector *)vector, capacity, sizeof(TYPE)); \
		}

	/**
	 * \def VECTOR_ISEMPTY
	 * \brief Wrapper routine for \a Vector_isempty routine.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \date 2020-05-05 Tue 07:21 AM
	 * \param TYPE C datatype
	 * \param NAME Name to give to generic object
	 */
	#define VECTOR_ISEMPTY(TYPE, NAME) \
		static bool * Vector##NAME##_isempty(const Vector##NAME * const vector) \
		{ \
			return Vector_isempty((Vector *)vector); \
		}

	/**
	 * \def VECTOR_ISSPACELESS
	 * \brief Wrapper routine for \a Vector_isspaceless routine.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \date 2020-05-05 Tue 07:21 AM
	 * \param TYPE C datatype
	 * \param NAME Name to give to generic object
	 */
	#define VECTOR_ISSPACELESS(TYPE, NAME) \
		static bool * Vector##NAME##_isspaceless(const Vector##NAME * const vector) \
		{ \
			return Vector_isspaceless((Vector *)vector); \
		}

	/**
	 * \def VECTOR_AT
	 * \brief Wrapper routine for \a Vector_at routine.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \date 2020-05-05 Tue 07:21 AM
	 * \param TYPE C datatype
	 * \param NAME Name to give to generic object
	 */
	#define VECTOR_AT(TYPE, NAME) \
		static TYPE * Vector##NAME##_at(const Vector##NAME * const vector, const size_t index) \
		{ \
			return (TYPE *)Vector_at((Vector *)vector, index, sizeof(TYPE)); \
		}

	/**
	 * \def VECTOR_GET
	 * \brief This macro uses \a Vector_at routine to get a value at a given index inside a given vector.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \date 2020-05-05 Tue 07:21 AM
	 * \param TYPE C datatype
	 * \param NAME Name to give to generic object
	 */
	#define VECTOR_GET(TYPE, NAME) \
		static TYPE Vector##NAME##_get(const Vector##NAME * const vector, const size_t index) \
		{ \
			return *((TYPE *)Vector_at((Vector *)vector, index, sizeof(TYPE))); \
		}

	/**
	 * \def VECTOR_SET
	 * \brief This macro uses \a Vector_at routine to set a value at a given index inside a given vector.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \date 2020-05-05 Tue 07:21 AM
	 * \param TYPE C datatype
	 * \param NAME Name to give to generic object
	 */
	#define VECTOR_SET(TYPE, NAME) \
		static void Vector##NAME##_set(Vector##NAME * const vector, const size_t index, TYPE value) \
		{ \
			*((TYPE *)Vector_at((Vector *)vector, index, sizeof(TYPE))) = value; \
		}

	/**
	 * \def VECTOR_SWAP
	 * \brief Wrapper routine for \a Vector_swap routine.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \date 2020-05-05 Tue 07:21 AM
	 * \param TYPE C datatype
	 * \param NAME Name to give to generic object
	 */
	#define VECTOR_SWAP(TYPE, NAME) \
		static void Vector##NAME##_swap(Vector##NAME## * const vector0, Vector##NAME * const vector1) \
		{ \
			Vector_swap((Vector *)vector0, (Vector *)vector1); \
		}

	/**
	 * \def VECTOR_COPY
	 * \brief Wrapper routine for \a Vector_copy routine.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \date 2020-05-05 Tue 07:21 AM
	 * \param TYPE C datatype
	 * \param NAME Name to give to generic object
	 */
	#define VECTOR_COPY(TYPE, NAME) \
		static Vector##NAME * Vector##NAME##_copy(const Vector##NAME * const vector, const size_t sizewise) \
		{ \
			return (Vector##NAME *)Vector_copy((Vector *)vector, sizewise, sizeof(TYPE)); \
		}

	/**
	 * \def VECTOR_RIGHT_SHIFT
	 * \brief Wrapper routine for \a Vector_right_shift routine.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \date 2020-05-05 Tue 07:21 AM
	 * \param TYPE C datatype
	 * \param NAME Name to give to generic object
	 */
	#define VECTOR_RIGHT_SHIFT(TYPE, NAME) \
		static void Vector##NAME##_right_shift(Vector##NAME * const vector) \
		{ \
			Vector_right_shift((Vector *)vector, sizeof(TYPE)); \
		}

	/**
	 * \def VECTOR_LEFT_SHIFT
	 * \brief Wrapper routine for \a Vector_right_shift routine.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \date 2020-05-05 Tue 07:21 AM
	 * \param TYPE C datatype
	 * \param NAME Name to give to generic object
	 */
	#define VECTOR_LEFT_SHIFT(TYPE, NAME) \
		static void Vector##NAME##_left_shift(Vector##NAME * const vector) \
		{ \
			Vector_left_shift((Vector *)vector, sizeof(TYPE)); \
		}

	/**
	 * \def VECTOR_CMP
	 * \brief Wrapper routine for \a Vector_cmp routine.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \date 2020-05-05 Tue 07:21 AM
	 * \param TYPE C datatype
	 * \param NAME Name to give to generic object
	 */
	#define VECTOR_CMP(TYPE, NAME) \
		static bool Vector##NAME##_cmp(const Vector##NAME * const vector0, const Vector##NAME * const vector1) \
		{ \
			return Vector_cmp((Vector *)vector0, sizeof(TYPE), (Vector *)vector1, sizeof(TYPE)); \
		}

	/**
	 * \def VECTOR_ISIN
	 * \brief Wrapper routine for \a Vector_isin routine.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \date 2020-05-05 Tue 07:21 AM
	 * \param TYPE C datatype
	 * \param NAME Name to give to generic object
	 */
	#define VECTOR_ISIN(TYPE, NAME) \
		static bool Vector##NAME##_isin(const Vector##NAME * const vector, const TYPE value) \
		{ \
			return Vector_isin((Vector *)vector, &value, sizeof(TYPE)); \
		}

	/**
	 * \def VECTOR_ISIN_OVERLAP
	 * \brief Wrapper routine for \a Vector_isin_overlap routine.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \date 2020-05-05 Tue 07:21 AM
	 * \param TYPE C datatype
	 * \param NAME Name to give to generic object
	 */
	#define VECTOR_ISIN_OVERLAP(TYPE, NAME) \
		static bool Vector##NAME##_isin_overlap(const Vector##NAME * const vector, const TYPE value) \
		{ \
			return Vector_isin_overlap((Vector *)vector, &value, sizeof(TYPE)); \
		}

	/**
	 * \def VECTOR_FIND
	 * \brief Wrapper routine for \a Vector_find routine.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \date 2020-05-05 Tue 07:21 AM
	 * \param TYPE C datatype
	 * \param NAME Name to give to generic object
	 */
	#define VECTOR_FIND(TYPE, NAME) \
		static TYPE * Vector##NAME##_find(const Vector##NAME * const vector, const TYPE value) \
		{ \
			return Vector_find((Vector *)vector, &value, sizeof(TYPE)); \
		}

	/**
	 * \def VECTOR_FIND_OVERLAP
	 * \brief Wrapper routine for \a Vector_find_overlap routine.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \date 2020-05-05 Tue 07:21 AM
	 * \param TYPE C datatype
	 * \param NAME Name to give to generic object
	 */
	#define VECTOR_FIND_OVERLAP(TYPE, NAME) \
		static TYPE * Vector##NAME##_find_overlap(const Vector##NAME * const vector, const TYPE value) \
		{ \
			return Vector_find_overlap((Vector *)vector, &value, sizeof(TYPE)); \
		}

	/**
	 * \def VECTOR_COUNT
	 * \brief Wrapper routine for \a Vector_count routine.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \date 2020-05-05 Tue 07:21 AM
	 * \param TYPE C datatype
	 * \param NAME Name to give to generic object
	 */
	#define VECTOR_COUNT(TYPE, NAME) \
		static size_t Vector##NAME##_count(const Vector##NAME * const vector, const TYPE value) \
		{ \
			return Vector_count((Vector *)vector, &value, sizeof(TYPE)); \
		}

	/**
	 * \def VECTOR_COUNT_OVERLAP
	 * \brief Wrapper routine for \a Vector_count_overlap routine.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \date 2020-05-05 Tue 07:21 AM
	 * \param TYPE C datatype
	 * \param NAME Name to give to generic object
	 */
	#define VECTOR_COUNT_OVERLAP(TYPE, NAME) \
		static size_t Vector##NAME##_count_overlap(const Vector##NAME * const vector, const TYPE value) \
		{ \
			return Vector_count_overlap((Vector *)vector, &value, sizeof(TYPE)); \
		}

	/**
	 * \def VECTOR_PUSH_BACK
	 * \brief Wrapper routine for \a Vector_push_back routine.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \date 2020-05-05 Tue 07:21 AM
	 * \param TYPE C datatype
	 * \param NAME Name to give to generic object
	 */
	#define VECTOR_PUSH_BACK(TYPE, NAME) \
		static void Vector##NAME##_push_back(Vector##NAME * const vector, const TYPE value) \
		{ \
			Vector_push_back((Vector *)vector, &value, sizeof(TYPE)); \
		}

	/**
	 * \def VECTOR_POP_BACK
	 * \brief Wrapper routine for \a Vector_pop_back routine.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \date 2020-05-05 Tue 07:21 AM
	 * \param TYPE C datatype
	 * \param NAME Name to give to generic object
	 */
	#define VECTOR_POP_BACK(TYPE, NAME) \
		static void Vector##NAME##_pop_back(Vector##NAME * const vector) \
		{ \
			Vector_pop_back((Vector *)vector, sizeof(TYPE)); \
		}

	/**
	 * \def VECTOR_INSERT
	 * \brief Wrapper routine for \a Vector_insert routine.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \date 2020-05-05 Tue 07:21 AM
	 * \param TYPE C datatype
	 * \param NAME Name to give to generic object
	 */
	#define VECTOR_INSERT(TYPE, NAME) \
		static void Vector##NAME##_insert(Vector##NAME * const vector, const TYPE value, const size_t index) \
		{ \
			Vector_insert((Vector *)vector, &value, index, sizeof(TYPE)); \
		}

	/**
	 * \def VECTOR_REMOVE
	 * \brief Wrapper routine for \a Vector_remove routine.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \date 2020-05-05 Tue 07:21 AM
	 * \param TYPE C datatype
	 * \param NAME Name to give to generic object
	 */
	#define VECTOR_REMOVE(TYPE, NAME) \
		static void Vector##NAME##_remove(Vector##NAME * const vector, const size_t index) \
		{ \
			Vector_remove((Vector *)vector, index, sizeof(TYPE)); \
		}

	/**
	 * \def VECTOR_PUSH_FRONT
	 * \brief Wrapper routine for \a Vector_push_front routine.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \date 2020-05-05 Tue 07:21 AM
	 * \param TYPE C datatype
	 * \param NAME Name to give to generic object
	 */
	#define VECTOR_PUSH_FRONT(TYPE, NAME) \
		static void Vector##NAME##_push_front(Vector##NAME * const vector, const TYPE value) \
		{ \
			Vector_push_front((Vector *)vector, &value, sizeof(TYPE)); \
		}

	/**
	 * \def VECTOR_POP_FRONT
	 * \brief Wrapper routine for \a Vector_pop_front routine.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \date 2020-05-05 Tue 07:21 AM
	 * \param TYPE C datatype
	 * \param NAME Name to give to generic object
	 */
	#define VECTOR_POP_FRONT(TYPE, NAME) \
		static void Vector##NAME##_pop_front(Vector##NAME * const vector) \
		{ \
			Vector_pop_front((Vector *)vector, sizeof(TYPE)); \
		}

	/**
	 * \def VECTOR_FREE
	 * \brief Wrapper routine for \a Vector_free routine.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \date 2020-05-05 Tue 07:21 AM
	 * \param TYPE C datatype
	 * \param NAME Name to give to generic object
	 */
	#define VECTOR_FREE(TYPE, NAME) \
		static void Vector##NAME##_free(Vector##NAME * * const vector) \
		{ \
			Vector_free((Vector * *)vector); \
		}

	/**
	 * \struct Vector
	 * \brief Represents a Vector object type.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 23 January 2020
	 * \var void * value
	 * Address of the dynamic array.
	 * \var size_t cardinal
	 * Number of element written inside the vector.
	 * \var size_t capacity
	 * Max number of element that the vector can store.
	 */
	typedef struct {
		void * value;
		size_t cardinal;
		size_t capacity;
	} Vector;

	/**
	 * \fn Vector * Vector_malloc(const size_t capacity, const size_t offset)
	 * \brief Allocates a vector.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 23 January 2020
	 * \param capacity Number of elements.
	 * \param offset Size of one element in byte.
	 * \return Address of the vector instance.
	 */
	Vector * Vector_malloc(const size_t capacity, const size_t offset);

	/**
	 * \fn Vector * Vector_realloc(Vector * vector, const size_t capacity, const size_t offset)
	 * \brief reallocates a vector.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 04 February 2020
	 * \param vector Given vector.
	 * \param capacity New capacity of the given vector.
	 * \param offset Size in byte of an element in the vector.
	 * \return NULL if error occurred or if capacity is 0 else the address of the reallocated vector.
	 */
	Vector * Vector_realloc(Vector * vector, const size_t capacity, const size_t offset);

	/**
	 * \fn void Vector_free(Vector * * const vector)
	 * \brief frees previously allocated vector
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 23 January 2019
	 * \param vector address of the pointer containing the vector to be freed.
	 */
	void Vector_free(Vector * * const vector);

	/**
	 * \fn bool Vector_isempty(const Vector * const vector)
	 * \brief Tests if a vector is empty.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 04 February 2020
	 * \param vector Given vector.
	 * \return true if no elements are written in vector else false. 
	 */
	bool Vector_isempty(const Vector * const vector);

	/**
	 * \fn bool Vector_isspaceless(const Vector * const vector)
	 * \brief Tests if a vector is spaceless.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 04 February 2020
	 * \param vector Given vector.
	 * \return true if vector takes no space in memory else false.
	 */
	bool Vector_isspaceless(const Vector * const vector);

	/**
	 * \fn void * Vector_at(const Vector * const vector, const size_t index, const size_t offset)
	 * \brief Returns the address of the element of given index in a given vector.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 04 February 2020
	 * \param vector Given vector.
	 * \param index Given index of an element in the vector.
	 * \param offset Size in byte of an element in the vector.
	 * \return Address of the element in vector of given index.
	 */
	void * Vector_at(const Vector * const vector, const size_t index, const size_t offset);

	/**
	 * \fn void Vector_swap(Vector * const vector0, Vector * const vector1)
	 * \brief Swaps the content of two given vectors
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-03-23 Mon 12:33 AM
	 * \param vector0 Given vector.
	 * \param vector1 Given vector.
	 */
	void Vector_swap(Vector * const vector0, Vector * const vector1);

	/**
	 * \fn Vector * Vector_copy(const Vector * const vector, const size_t sizewise, const size_t offset)
	 * \brief Creates a copy of a given vector.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-03-29 Sun 11:02 PM
	 * \param vector Given vector.
	 * \param sizewise Number of bytes to copy from the value of the given vector.
	 * \param offset Size in byte of an element in the vector.
	 * \return Newly allocated copy of a given vector.
	 */
	Vector * Vector_copy(const Vector * const vector, const size_t sizewise, const size_t offset);

	/**
	 * \fn void Vector_right_shift(Vector * const vector, const size_t offset)
	 * \brief Shifts all vector values from one element size to the right.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 23 January 2020
	 * \param vector Given vector.
	 * \param offset Size of one element in byte.
	 */
	void Vector_right_shift(Vector * const vector, const size_t offset);

	/**
	 * \fn void Vector_left_shift(Vector * const vector, const size_t offset)
	 * \brief Shifts all vector values from one element size to the left.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 23 January 2020
	 * \param vector Given vector.
	 * \param offset Size of one element in byte.
	 */
	void Vector_left_shift(Vector * const vector, const size_t offset);

	/**
	 * \fn bool Vector_cmp(const Vector * const vector0, const size_t offset0, const Vector * const vector1, const size_t offset1);
	 * \brief Compares two vectors.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 23 January 2020
	 * \param vector0 Given vector.
	 * \param offset0 Size of one element (in byte) of first given vector.
	 * \param vector1 Given vector.
	 * \param offset1 Size of one element (in byte) of second given vector.
	 * \return true if vector0 and vector1 contain same element(s) in same order else false.
	 */
	bool Vector_cmp(const Vector * const vector0, const size_t offset0, const Vector * const vector1, const size_t offset1);

	/**
	 * \fn bool Vector_isin(const Vector * const vector, const void * const sequence, const size_t offset)
	 * \brief Tests if a given element is in a given vector with a variable jump of a given offset size.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 04 February 2020
	 * \param vector Given vector.
	 * \param sequence Address of a given element.
	 * \param offset Size in byte of an element in the vector.
	 * \return true if a given element is in a given vector else false.
	 */
	bool Vector_isin(const Vector * const vector, const void * const sequence, const size_t offset);

	/**
	 * \fn bool Vector_isin_overlap(const Vector * const vector, const void * const sequence, const size_t offset)
	 * \brief Tests if a given element is in a given vector with a constant jump of one byte.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 04 February 2020
	 * \param vector Given vector.
	 * \param sequence Address of a given element.
	 * \param offset Size in byte of an element in the vector.
	 * \return true if a given element is in a given vector else false.
	 */
	bool Vector_isin_overlap(const Vector * const vector, const void * const value, const size_t offset);

	/**
	 * \fn void * Vector_find(const Vector * const vector, void * const value, const size_t offset)
	 * \brief Searches a given element in a given vector with a variable jump of a given offset size,  if found returns its address.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-03-03 Tue 12:22 AM
	 * \param vector Given vector.
	 * \param value Address of a given element.
	 * \param offset Size in byte of an element in the vector.
	 * \return NULL if nothing was found else address of the element found.
	 */
	void * Vector_find(const Vector * const vector, void * const value, const size_t offset);

	/**
	 * \fn void * Vector_find_overlap(const Vector * const vector, void * const value, const size_t offset)
	 * \brief Searches a given element in a given vector with a variable jump of a given offset size,  if found returns its address.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-03-03 Tue 12:22 AM
	 * \param vector Given vector.
	 * \param value Address of a given element.
	 * \param offset Size in byte of an element in the vector.
	 * \return NULL if nothing was found else address of the element found.
	 */
	void * Vector_find_overlap(const Vector * const vector, void * const value, const size_t offset);

	/**
	 * \fn size_t Vector_count(const Vector * const vector, const void * const sequence, const size_t offset)
	 * \brief Counts occurrences of a given element in a given vector with a variable jump of given offset size.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 04 February 2020
	 * \param vector Given vector.
	 * \param sequence Address of a given element.
	 * \param offset Size in byte of an element in the vector.
	 * \return Number of occurrences.
	 */
	size_t Vector_count(const Vector * const vector, const void * const sequence, const size_t offset);

	/**
	 * \fn size_t Vector_count_overlap(const Vector * const vector, const void * const sequence, const size_t offset)
	 * \brief Counts occurrences of a given element in a given vector with a constant jump of one byte. 
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 04 February 2020
	 * \param vector Given vector.
	 * \param sequence Address of a given element.
	 * \param offset Size in byte of an element in the vector.
	 * \return Number of occurrences.
	 */
	size_t Vector_count_overlap(const Vector * const vector, const void * const sequence, const size_t offset);

	/**
	 * \fn void Vector_push_back(Vector * const vector, const void * const value, const size_t offset)
	 * \brief Adds an element behind the last element, reallocates the vector if not enough space.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 04 February 2020
	 * \param vector Given vector.
	 * \param value Given value to set an element in vector.
	 * \param offset Size in byte of an element in the vector.
	 */
	void Vector_push_back(Vector * const vector, const void * const value, const size_t offset);

	/**
	 * \fn void Vector_pop_back(Vector * const vector, const size_t offset)
	 * \brief Removes the last element of the vector, if the vector is empty but not space less then it reallocates the vector of -1 element space.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 04 February 2020
	 * \param vector Given vector.
	 * \param offset Size in byte of an element in the vector.
	 */
	void Vector_pop_back(Vector * const vector, const size_t offset);

	/**
	 * \fn void Vector_insert(Vector * const vector, const void * value, const size_t index, const size_t offset)
	 * \brief Adds element at given position.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-03-03 Tue 12:06 AM
	 * \param vector Given vector.
	 * \param value Given value to set an element in vector.
	 * \param index Given index.
	 * \param offset Size in byte of an element in the vector.
	 */
	void Vector_insert(Vector * const vector, const void * value, const size_t index, const size_t offset);

	/**
	 * \fn void Vector_remove(Vector * const vector, const size_t index, const size_t offset)
	 * \brief Removes the element of given index.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-03-03 Tue 12:12 AM
	 * \param vector Given vector.
	 * \param index Given index of an element in the vector.
	 * \param offset Size in byte of an element in the vector.
	 */
	void Vector_remove(Vector * const vector, const size_t index, const size_t offset);

	/**
	 * \fn void Vector_push_front(Vector * const vector, const void * const value, const size_t offset)
	 * \brief Adds an element in front of the first element, reallocates the vector if not enough space.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 04 February 2020
	 * \param vector Given vector.
	 * \param value Given value to set an element in vector.
	 * \param offset Size in byte of an element in the vector.
	 */
	void Vector_push_front(Vector * const vector, const void * const value, const size_t offset);

	/**
	 * \fn void Vector_pop_front(Vector * const vector, const size_t offset)
	 * \brief Removes the first element of the vector, if the vector is empty but not space less then it reallocates the vector of -1 element space.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 04 February 2020
	 * \param vector Given vector.
	 * \param offset Size in byte of an element in the vector.
	 */
	void Vector_pop_front(Vector * const vector, const size_t offset);
#endif
