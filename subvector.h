/**
 * \file subvector.h
 * \brief generic sub vector library allowing indexed access to vectors.
 * \author sangsoic <sangsoic@protonmail.com>
 * \version 0.1
 * \date 2020-07-26 Wed 06:00 AM
 * \copyright 
 *
 * Copyright 2020 Sangsoic library author
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *             http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef SUBVECTOR_H
	#define SUBVECTOR_H

	#include <stdio.h>
	#include <stdlib.h>
	#include <math.h>

	#include "vector.h"

	/**
	 * \def SUBVECTOR
	 * \brief Sub vector generic structure.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \date 2020-05-05 Tue 07:19 AM
	 * \param TYPE C datatype
	 * \param NAME Name to give to generic object
	 */
	#define SUBVECTOR(TYPE, NAME) \
		struct SubVector##NAME \
		{ \
			TYPE * startAt; \
			ssize_t (* index)(const size_t); \
			size_t length; \
		}; \
		typedef struct SubVector##NAME SubVector##NAME;

	/**
	 * \def SUBVECTOR_EXTRACT
	 * \brief Wrapper routine for \a SubVector_extract routine.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \date 2020-05-05 Tue 07:21 AM
	 * \param TYPE C datatype
	 * \param NAME Name to give to generic object
	 */
	#define SUBVECTOR_EXTRACT(TYPE, NAME) \
		SubVector##NAME * SubVector##NAME##_extract(const size_t start, ssize_t (* index)(const size_t), const size_t length, const Vector##NAME * const from, SubVector##NAME * const to) \
		{ \
			return (SubVector##NAME *)SubVector_extract(start, index, length, (Vector *)from, (SubVector *)to, sizeof(TYPE)); \
		}

	/**
	 * \def SUBVECTOR_AT
	 * \brief Wrapper routine for \a Vector_at routine.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \date 2020-05-05 Tue 07:21 AM
	 * \param TYPE C datatype
	 * \param NAME Name to give to generic object
	 */
	#define SUBVECTOR_AT(TYPE, NAME) \
		TYPE * SubVector##NAME##_at(const SubVector##NAME * const sub, const size_t index) \
		{ \
			return (TYPE *)SubVector_at((SubVector *)sub, index, sizeof(TYPE)); \
		}

	/**
	 * \def SUBSUBVECTOR_EXTRACT
	 * \brief Wrapper routine for \a SubSubVector_extract routine.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \date 2020-05-05 Tue 07:21 AM
	 * \param TYPE C datatype
	 * \param NAME Name to give to generic object
	 */
	#define SUBSUBVECTOR_EXTRACT(TYPE, NAME) \
		SubVector##NAME * SubSubVector##NAME##_extract(const size_t start, ssize_t (* index)(const size_t), const size_t length, const SubVector##NAME * const from, SubVector##NAME * to) \
		{ \
			return (SubVector##NAME *)SubSubVector_extract(start, index, length, (SubVector *)from, (SubVector *)to, sizeof(TYPE)); \
		}

	/**
	 * \def SUBVECTOR_GET
	 * \brief Wrapper routine for \a SubVector_get routine.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \date 2020-05-05 Tue 07:21 AM
	 * \param TYPE C datatype
	 * \param NAME Name to give to generic object
	 */
	#define SUBVECTOR_GET(TYPE, NAME) \
		TYPE SubVector##NAME##_get(const SubVector##NAME * const sub, const size_t index) \
		{ \
			return *((TYPE *)SubVector_at((SubVector *)sub, index, sizeof(TYPE))); \
		}

	/**
	 * \def SUBVECTOR_SET
	 * \brief Wrapper routine for \a SubVector_set routine.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \date 2020-05-05 Tue 07:21 AM
	 * \param TYPE C datatype
	 * \param NAME Name to give to generic object
	 */
	#define SUBVECTOR_SET(TYPE, NAME) \
		void SubVector##NAME##_set(const SubVector##NAME * const sub, const size_t index, const TYPE value) \
		{ \
			return *((TYPE *)SubVector_at((SubVector *)sub, index, sizeof(TYPE))) = value; \
		}

	/**
	 * \def SUBVECTOR_FREE
	 * \brief Wrapper routine for \a SubVector_free routine.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \date 2020-05-05 Tue 07:21 AM
	 * \param TYPE C datatype
	 * \param NAME Name to give to generic object
	 */
	#define SUBVECTOR_FREE(TYPE, NAME) \
		void SubVector##NAME##_free(SubVector##NAME * * const sub) \
		{ \
			SubVector_free((SubVector * *)sub); \
		}

	/**
	 * \struct SubVector
	 * \brief Represents a SubVector object type.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 23 January 2020
	 * \var void * startAt
	 * Address of a parent vector element.
	 * \var ssize_t (* index)(const size_t)
	 * Any sequence that maps the set of natural numbers {0, 1, 2, ...} to a set of integers.
	 * \var size_t capacity
	 * Number of elements pointed by the sub vector.
	 */
	struct SubVector
	{
		void * startAt;
		ssize_t (* index)(const size_t);
		size_t length;
	};

	typedef struct SubVector SubVector;


	/**
	 * \fn void SubVector_free(SubVector * * const sub)
	 * \brief Frees memory taken by a given \a SubVector object.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-08-07 Fri 10:05 PM
	 * \param Address of the pointer containing the address of a \a SubVector object.
	 */
	void SubVector_free(SubVector * * const sub);

	/**
	 * \fn SubVector * SubVector_extract(const size_t start, ssize_t (* index)(const size_t), const size_t length, const Vector * const from, SubVector * to, const size_t offset)
	 * \brief Extracts a \a SubVector from a \a Vector object.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-08-07 Fri 11:14 PM
	 * \param start Index of an element of a \a Vector object.
	 * \param index Sequence mapping natural numbers to the set of index of the extracted elements relative to the \a start address.
	 * \param length Number of elements to extract.
	 * \param from \a Vector from which \a Vector elements are extracted.
	 * \param to \a SubVector to which \a Vector elements are extracted. if NULL is given then no \a SubVector will be freed.
	 * \param offset Size of one element in byte.
	 * \return Newly allocated and extracted \a SubVector.
	 */
	SubVector * SubVector_extract(const size_t start, ssize_t (* index)(const size_t), const size_t length, const Vector * const from, SubVector * to, const size_t offset);

	/**
	 * \fn void * SubVector_at(const SubVector * const sub, const size_t i, const size_t offset)
	 * \brief Returns the address of the element of given index in a given vector.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-08-07 Fri 11:55 PM
	 * \param sub Given \a SubVector.
	 * \param i Given index of an element of the \a sub \a SubVector
	 * \param offset Size in byte of an element in the vector.
	 * \return Address of the element in \a sub of given index.
	 */
	void * SubVector_at(const SubVector * const sub, const size_t i, const size_t offset);

	/**
	 * \fn SubVector * SubSubVector_extract(const size_t start, ssize_t (* index)(const size_t), const size_t length, const SubVector * const from, SubVector * to, const size_t offset)
	 * \brief Extracts a \a SubVector from another \a SubVector object.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-08-07 Fri 12:20 AM
	 * \param start Index of an element of a \a Vector object.
	 * \param index Sequence mapping natural numbers to the set of index of the extracted elements relative to the \a start address.
	 * \param length Number of elements to extract.
	 * \param from \a SubVector from which \a SubVector elements are extracted.
	 * \param to \a SubVector to which \a SubVector elements are extracted. if NULL is given then no \a SubVector will be freed.
	 * \param offset Size of one element in byte.
	 * \return Newly allocated and extracted \a SubVector.
	 */
	SubVector * SubSubVector_extract(const size_t start, ssize_t (* index)(const size_t), const size_t length, const SubVector * const from, SubVector * to, const size_t offset);

#endif
